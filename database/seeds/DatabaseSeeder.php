<?php

use Illuminate\Database\Seeder;
use \App\User;
use \App\Model\Category;
use \App\Model\Question;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(StateSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(CountrySeeder::class);
    }
}
