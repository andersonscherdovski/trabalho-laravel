<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CityRequest;
use App\Http\Controllers\Controller;
use App\Model\City;
use Symfony\Component\HttpFoundation\Response;

class CityController extends Controller
{
    public function index()
    {
        return City::latest()->get();
    }

    public function store(CityRequest $request)
    {
        City::create(array(
            'name' => $request->name,
            'state_id' => $request->state_id,
        ));

        return response('Criado', Response::HTTP_CREATED);
    }

    public function show(City $city)
    {
        return $city;
    }

    public function update(CityRequest $request, City $city)
    {
        $city->update(
            [
                'name' => $request->name,
                'state_id' => $request->state_id,
            ]
        );

        return response('Atualizado', Response::HTTP_ACCEPTED);
    }

    public function destroy(City $city)
    {
        $city->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
