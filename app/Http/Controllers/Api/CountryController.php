<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CountryRequest;
use App\Http\Controllers\Controller;
use App\Model\Country;
use Symfony\Component\HttpFoundation\Response;

class CountryController extends Controller
{
    public function index()
    {
        return Country::latest()->get();
    }

    public function store(CountryRequest $request)
    {
        Country::create(array(
            'name' => $request->name,
            'code' => $request->code,
        ));

        return response('Criado', Response::HTTP_CREATED);
    }

    public function show(Country $country)
    {
        return $country;
    }

    public function update(CountryRequest $request, Country $country)
    {
        $country->update(
            [
                'name' => $request->name,
                'slug' => str_slug($request->name)
            ]
        );

        return response('Atualizado', Response::HTTP_ACCEPTED);
    }

    public function destroy(Country $country)
    {
        $country->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
