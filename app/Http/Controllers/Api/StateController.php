<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StateRequest;
use App\Http\Controllers\Controller;
use App\Model\State;
use Symfony\Component\HttpFoundation\Response;

class StateController extends Controller
{
    public function index()
    {
        return State::latest()->get();
    }

    public function store(StateRequest $request)
    {
        State::create(array(
            'name' => $request->name,
            'code' => $request->code,
            'country_id' => $request->country_id,
        ));

        return response('Criado', Response::HTTP_CREATED);
    }

    public function show(State $state)
    {
        return $state;
    }

    public function update(StateRequest $request, State $state)
    {
        $state->update(
            [
                'name' => $request->name,
                'code' => $request->code,
                'country_id' => $request->country_id,
            ]
        );

        return response('Atualizado', Response::HTTP_ACCEPTED);
    }

    public function destroy(State $state)
    {
        $state->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
