<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Base\Models\BaseModel;

/**
 * Class State
 *
 * A classe estende BaseModel
 * @package BaseModel
 *
 */
class State extends Model
{

    /**
     * Seta qual databaser connection será usada.
     *
     * @var protected $connection
     */
    protected $connection = 'mysql';

    /**
     * Seta qual tabela será usada.
     *
     * @var protected $table
     */
    protected $table = 'state';

    /**
     * Seta qual as colunas serão usadas.
     *
     * @var protected $fillable
     */
    protected $fillable = [
        'name',
        'code',
        'country_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {

        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {

        return $this->hasMany(City::class);
    }
}
