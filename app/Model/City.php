<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 *
 * A classe estende BaseModel
 * @package BaseModel
 *
 */
class City extends Model
{

    /**
     * Seta qual databaser connection será usada.
     *
     * @var protected $connection
     */
    protected $connection = 'mysql';

    /**
     * Seta qual tabela será usada.
     *
     * @var protected $table
     */
    protected $table = 'city';

    /**
     * Seta qual as colunas serão usadas.
     *
     * @var protected $fillable
     */
    protected $fillable = [
        'name',
        'state_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

}
