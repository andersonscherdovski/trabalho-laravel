<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Base\Models\BaseModel;

/**
 * Class Country
 *
 * A classe estende BaseModel
 * @package BaseModel
 *
 */
class Country extends Model
{

    /**
     * Seta qual databaser connection será usada.
     *
     * @var protected $connection
     */
    protected $connection = 'mysql';

    /**
     * Seta qual tabela será usada.
     *
     * @var protected $table
     */
    protected $table = 'country';

    /**
     * Seta qual as colunas serão usadas.
     *
     * @var protected $fillable
     */
    protected $fillable = [
        'name',
        'code'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(State::class);
    }
}
